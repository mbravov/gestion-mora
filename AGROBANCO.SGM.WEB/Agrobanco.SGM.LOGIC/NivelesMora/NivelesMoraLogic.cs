﻿using Agrobanco.SGM.MODELS.Response;
using Agrobanco.SGM.REPOSITORIES.NivelesMora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SGM.LOGIC.NivelesMora
{
    public class NivelesMoraLogic
    {
        private readonly NivelesMoraRepositories oRepositories = new NivelesMoraRepositories();

        public ListaAgencias listarAgencias(string cAgencia)
        {
            ListaAgencias lAgencias = new ListaAgencias();
            lAgencias = oRepositories.listarAgencias(cAgencia);
            return lAgencias;
        }
        public void ActualizarNivelesMora(string cCodAgencia, decimal nMora, string cUser)
        {
            oRepositories.ActualizarMora(cCodAgencia, nMora, cUser);
        }

        public void RegistrarHitoricoMora()
        {
            oRepositories.RegistrarHitoricoMora();
        }
    }
}
