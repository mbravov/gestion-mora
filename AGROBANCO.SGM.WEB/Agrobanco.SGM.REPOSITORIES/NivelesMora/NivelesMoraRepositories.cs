﻿using Agrobanco.SGM.MODELS.Response;
using System;
using System.Collections.Generic;
using AGROBANCO.SGM.REPOSITORIES.DataDb2;
using AGROBANCO.SGM.REPOSITORIES;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBM.Data.DB2.iSeries;

namespace Agrobanco.SGM.REPOSITORIES.NivelesMora
{
    public class NivelesMoraRepositories
    {
        public NivelesMoraRepositories()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;
        public ListaAgencias listarAgencias(string cAgencia)
        {

            ListaAgencias lAgencias = new ListaAgencias();

                _db2DataContext.RunStoredProcedure(library + ".UP_SGM_LISTAR_MORA_AGENCIA", out IDataReader reader);

                while (reader.Read())
                {
                    ResponseAgenciaMora oAgencia = new ResponseAgenciaMora();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_AGENCIA")))
                    {
                        oAgencia.cCodAgencia = reader.GetString("CODIGO_AGENCIA");
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_AGENCIA")))
                    {
                        oAgencia.cAgencia = reader.GetString("NOMBRE_AGENCIA");
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("VALOR_MORA")))
                    {
                        oAgencia.nMora = reader.GetDecimal("VALOR_MORA");
                    }

                    lAgencias.AddAgencia(oAgencia);
                }

            return lAgencias;

        }

        public void ActualizarMora(string cCodAgencia, decimal nMora, string cUser)
        {
            string Fecha = DateTime.Now.ToString("yyyyMMdd");
            string Hora = DateTime.Now.ToString("HHmmss");

            var lDataParam = new iDB2Parameter[5];

            lDataParam[0] = _db2DataContext.CreateParameter("P_AGENCIA",3,cCodAgencia);
            lDataParam[1] = _db2DataContext.CreateParameter("P_MORA", 5, nMora.ToString());
            lDataParam[2] = _db2DataContext.CreateParameter("P_FECHA", 8, Fecha);
            lDataParam[3] = _db2DataContext.CreateParameter("P_HORA", 6, Hora);
            lDataParam[4] = _db2DataContext.CreateParameter("P_USUARIO", 20, cUser);


            _db2DataContext.RunStoredProcedure(library + ".UP_SGM_ACTUALIZAR_NIVELESMORA", lDataParam);


        }

        public void RegistrarHitoricoMora()
        {
            _db2DataContext.RunStoredProcedure(library + ".UP_SGM_REGISTRAR_HISTORICO_MORA");

        }
    }
}
