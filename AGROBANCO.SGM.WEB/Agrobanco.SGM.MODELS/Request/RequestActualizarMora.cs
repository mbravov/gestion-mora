﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SGM.MODELS.Request
{
    public class RequestActualizarMora
    {
        public string cCodAgencia { get; set; }
        public decimal nMora { get; set; }
    }
}
