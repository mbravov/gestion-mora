﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SGM.MODELS.Response
{
    public class ResponseAgenciaMora
    {
        public string cCodAgencia { get; set; }
        public string cAgencia { get; set; }
        public decimal nMora { get; set; }
    }
    public class ListaAgencias
    {
        public List<ResponseAgenciaMora> lAgencias { get; set; }
        public ListaAgencias()
        {
            lAgencias = new List<ResponseAgenciaMora>();
        }

        public void AddAgencia(ResponseAgenciaMora a)
        {
            lAgencias.Add(a);
        }
    }
}
