﻿using Agrobanco.SGM.WEB.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SGM.WEB.Filters
{
    public class VerificaSession : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            //General general = new General();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpper();
            bool validate = true;

            switch (controllerName)
            {
                case "ERROR":
                    validate = false;
                    break;
                case "REDIRECT":
                    validate = false;
                    break;
                default:
                    validate = true;
                    break;
            }

            SeguridadHelper helper = new SeguridadHelper();

            if (validate == true)
            {
                string Token = (string)HttpContext.Current.Session["TokenSCC"];
                string idPerfil = (string)HttpContext.Current.Session["IdPerfilSCC"];

                if (Token == null)
                {
                    //filterContext.HttpContext.Response.Redirect("/Redirect/RedirectExternalUrl?opcionRedirect=1");
                    filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?opcionRedirect=1");
                }
                else if (helper.ValidarTokenActivo(Token) == false)
                {
                    string typeResponse = ((ReflectedActionDescriptor)filterContext.ActionDescriptor).MethodInfo.ToString().ToUpper();
                    if (typeResponse.Contains("ACTIONRESULT"))
                    {
                        filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?opcionRedirect=2");
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?opcionRedirect=3");
                    }
                    //filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?opcionRedirect=2");

                }
                else
                {
                    ResultadoObtenerModel<Usuario> UsuarioSesion = helper.ObtenerSesionUsuario(Token, idPerfil);

                    //EloginResponse UsuarioSGS = helper.ObtenerUsuario("", Token);
                    //EloginResponse SesionActual = (EloginResponse)HttpContext.Current.Session["UsuarioSCC"];
                    //if (SesionActual != null)
                    //{
                    //    UsuarioSGS.vPerfilDescripcion = SesionActual.vPerfilDescripcion;
                    //}
                    HttpContext.Current.Session["UsuarioSCC"] = UsuarioSesion;
                    //VALIDAR AD
                    if (UsuarioSesion.Model.EstadoAD == "1")
                    {
                        ServicioResponse eServicioResponse = helper.ValidarUsuarioAD(Token, UsuarioSesion.Model.UsuarioAD);
                        if (eServicioResponse.Resultado == 0)
                        {
                            filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?msjErrorExcepcion=" + eServicioResponse.Mensaje);
                        }
                    }

                }
            }
        }
    }
}