﻿$(document).ready(function () {

    const urlBase = $("#UrlBase").val();

    $("#txtAgencia").on('keyup', function () {

        var cAgencia = $("#txtAgencia").val().toUpperCase().trim();

        if (cAgencia == "") {
            $("#det div").each(function () {

                var id = $(this).attr("valor");
                $("#" + id).show();
                //$("#in-" + id).attr("bEstado", "si");

            });
        } else {
            $("#det div").find('label').each(function () {

                codigo = $(this).html();

                var Encontrado = codigo.includes(cAgencia);

                if (Encontrado) {

                    var id = $(this).attr("valor");
                    $("#" + id).show();
                    //$("#in-" + id).attr("bEstado", "si");

                } else {
                    var id = $(this).attr("valor");
                    $("#" + id).hide();
                    //$("#in-" + id).attr("bEstado", "no");

                }

            });
        }

    });

    function Listar() {
        var sContenido = "";
        if (lAgencias != null && lAgencias.length > 0) {
            lAgencias.forEach(function (item) {
                sContenido += "<div class='row no-pad-bot' valor=" + item.cCodAgencia + " style='padding: 0px;' id=" + item.cCodAgencia + ">";
                sContenido += "<div class='col s12 m12 row no-pad-bot' style='margin-bottom: 0px;'>";
                sContenido += "<div class='col s8 m8'><label style='color: black;' class='fila-select'  valor=" + item.cCodAgencia + " value=" + item.cAgencia + ">" + item.cAgencia + "</label></div>";
                sContenido += "<div class='col s4 m4'><input type='number' step='0.00' required  id=" + 'in-' + item.cCodAgencia + " bEstado='si' value=" + item.nMora + " />";
                sContenido += "<span id=" + 'nombresModal-error-' + item.cCodAgencia +" class='span-error' style='display: none; color:#F44336;'>Este campo es obligatorio.</span></div>";
                sContenido += "</div>";
                sContenido += "</div>";
            });
        }
        $("#det").html(sContenido);
    }

    Listar();

    function ObtenerAgenciasActualizar() {
        var lMora = [];
        var Filas = $(".fila-select");
        for (var i = 0; i < Filas.length; i++) {
            var Fila = Filas.eq(i);
            var cCodAgencia = Fila.attr("valor");
            var sName = "in-" + cCodAgencia;
            var nMora = $("#" + sName + "").val();

            //if ($("#in-" + cCodAgencia + "").attr('bEstado') == "si") {
                lMora.push({ cCodAgencia: cCodAgencia, nMora: nMora });
            //}
        }
        return lMora;
    }

    function ValidarDatos() {
        
        var validate = true;
        var Filas = $(".fila-select");
        for (var i = 0; i < Filas.length; i++) {
            var Fila = Filas.eq(i);
            var cCodAgencia = Fila.attr("valor");
            var sName = "in-" + cCodAgencia;

            //if ($("#in-" + cCodAgencia + "").attr('bEstado') == "si") {

                if (!$.fn.ValidarInput({ html: "#" + sName })) {
                    $("#" + sName).addClass("invalid");
                    $("#nombresModal-error-" + cCodAgencia).show();
                    validate = false;


                    $("#det div").each(function () {

                        var id = $(this).attr("valor");
                        $("#" + id).show();

                    });


                } else {
                    $("#" + sName).removeClass("invalid");
                    $("#nombresModal-error-" + cCodAgencia).hide();
                }                

            //}
        }




        return validate;

    }


    $("#btnProcesar").click(function (e) {
        e.preventDefault();
        if (!ValidarDatos()) {
            $('.p-desc-error').text("Por favor ingrese los campos obligatorios.");
            $('#ptv-alert-error').modal('open');
            return;
        }
      

        var textoMsg = "¿Estás seguro de actualizar los niveles de mora de todas las agencias?"
        $('.p-desc-confirmacion').text(textoMsg);
        let modalConfirmacionRegUsuario = $('#ptv-alert-confirmacion').modal({ dismissible: false });
        modalConfirmacionRegUsuario.modal('open');
    });

    $("#btnAgree").on('click', function () {

        
        var sMoraActualizar = ObtenerAgenciasActualizar();
        
            $("#modal_loading_agro").attr("style", "display:block");
        
        
        $.ajax({
            url: urlBase +'/ActualizarMora/ActualizarNivelesMora',
            type: 'POST',
            data: { sMoraActualizar: JSON.stringify(sMoraActualizar) },
            success: function (result, state) {
                var nEstado = result.nEstado;

                $("#modal_loading_agro").attr("style", "display:none");

                if (nEstado == 1) {
                    var nFilas = result.nFilas;
                    $('#ptv-alert-ok').modal({
                        onCloseEnd: function () {
                            location.reload();
                        }
                    });
                    $('.p-descripcion').text("Datos actualizados correctamente!");
                    $('#ptv-alert-ok').modal('open');


                } else {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                        }
                    });
                    $('.p-desc-error').text("Ha ocurrido un error en la aplicación");
                    $('#ptv-alert-error').modal('open');
                }

            },
            error: function (e, b, c) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text("Ha ocurrido un error en la aplicación.");
                $('#ptv-alert-error').modal('open');
                $("#modal_loading_agro").attr("style", "display:none");
                return false;
            }
        });


    });


});

