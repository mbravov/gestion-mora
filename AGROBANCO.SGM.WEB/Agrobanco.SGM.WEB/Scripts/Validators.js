﻿(function ($) {
    $.fn.InputValidator = function (m) {
        m.dom = m.dom || "";
        m.longitud = m.longitud || 2;
        m.onlynumber = m.onlynumber || false;
        m.validate = m.validate || false;
        m.min = m.min || 0;
        m.max = m.max || 1000;

        $(m.dom).attr("maxLength", m.longitud);
        $(m.dom).unbind("paste").bind("paste", function (e) {
            var Yo = $(this);
            setTimeout(function () {
                if (m.onlynumber && isNaN(parseInt(Yo.val()))) {
                    Yo.val("");
                }
            }, 50);
            return true;
        });

        $(m.dom).unbind("drop").bind("drop", function (e) {
            return false;
        });
        $(m.dom).unbind("change").change(function () {
            var yo = $(this);
            var value = yo.val();
            if (m.onlynumber) {
                if (m.validate) {
                    var val = yo.val();
                    if (val > m.max) { val = m.max; }
                    if (val < m.min) { val = m.min; }
                    yo.val(val);
                }
            }
            setTimeout(function () {
                if (yo.val().toString().length >= m.longitud) {
                    yo.val(yo.val().substring(0, m.longitud));
                }
            }, 40);
        });
        $(m.dom).unbind("keypress").keypress(function (e) {
            var tecla = (document.all) ? e.keyCode : e.which;
            var patron = /[0-9]/;

            if (m.onlynumber && !patron.test(String.fromCharCode(tecla)) && !(tecla == 8 || tecla == 13 || tecla == 0)) {
                //if (){
                return false;
                //}
            }
            return true;
        });
    }
})(jQuery);