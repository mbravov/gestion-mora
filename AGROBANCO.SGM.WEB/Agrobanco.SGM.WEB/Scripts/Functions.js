﻿(function ($) {
    $.fn.ValidarInput = function (m) {
        m.html = m.html || "";
        m.isSelect = m.isSelect || false;

        if ($(m.html).val() == null) {
            return false;
        }
        if (m.isSelect) {
            return $(m.html).val() != -1;
        } else {
            return $(m.html).val().length != 0
        }
    }
    $.fn.NumericInput = function (m) {
        m.html = m.html || "";
        m.length = m.length || 1;

        $(m.html).unbind("keypress").keypress(function (e) {

        });
    }
    $.fn.Ajax = function (m) {
        m.url = m.url || "";
        m.tipo = m.tipo || "post";
        m.datos = m.datos || {};
        m.test = m.test || false;
        m.response = m.response || null;

        $.ajax({
            url: m.url,
            type: m.tipo,
            data: m.datos,
            success: function (a, e) {
                if (m.response != null) { m.response(a, e); }
            },
            error: function (a, e) {
                if (m.test) {
                    $("body").html(a.responseText);
                }
            }
        });
    }
    $.fn.SubmitForm = function (m) {
        m.tipo = m.tipo || "post";
        m.datos = m.datos || {};
        m.url = m.url || "/";

        var Cadena = "<form id='_tmpForm_' method='" + m.tipo + "' action='" + m.url + "' style='display: none'>";
        if (m.datos.length > 0) {
            m.datos.forEach(function (item) {
                Cadena += "<input name='" + item.name + "' value='" + item.value + "'/>";
            });
        }
        Cadena += "</form>";

        $("body").append(Cadena);
        $("#_tmpForm_").submit();
    }
})(jQuery);

function BloquearCarga(mensaje) {
    var html = "";
    if (!mensaje) {
        mensaje = "Procesando...";
    }
    html = ('<div id="vntEspera" class="modal fade espera" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true"> <div class="modal-dialog modal-sm" style="margin-top: 47vh;"> <div class="modal-content"> <div class="row"> <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 10px;"> <i class="fa fa-cogs" style="margin-left: 15px; color: gray"></i>' +
        '</div> <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"> <div class="row"> <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color: #D9534F"> <b>Por favor espere</b> </div> <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <b>' + mensaje + '</b> </div> </div> </div> </div> </div> </div> </div>');
    $(html).appendTo('body');
    $("#vntEspera").modal('show');
}

function DesbloquearCarga() {
    $("#vntEspera").modal('hide');
    setTimeout(function () { $("div#vntEspera").remove(); }, 300);
}