﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SGM.WEB.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult UnauthorizedOperation(string operacion, string modulo, string msjErrorExcepcion)
        {

            ViewBag.operacion = operacion;
            ViewBag.modulo = modulo;
            ViewBag.msjErrorExcepcion = msjErrorExcepcion;
            return View();
        }
    }
}