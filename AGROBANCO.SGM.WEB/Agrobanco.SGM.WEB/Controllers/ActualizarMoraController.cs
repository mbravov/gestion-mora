﻿using Agrobanco.SGM.LOGIC.NivelesMora;
using Agrobanco.SGM.MODELS.Request;
using Agrobanco.SGM.MODELS.Response;
using Agrobanco.SGM.WEB.Helpers;
using AGROBANCO.SGM.HELPERS.Log;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SGM.WEB.Controllers
{
    public class ActualizarMoraController : Controller
    {
        public static string UrlBase => ConfigurationManager.AppSettings["UrlBase"];
        private readonly NivelesMoraLogic oLogic = new NivelesMoraLogic();
        private readonly Log oLog = new Log();


        public ActionResult ActualizarMora()
        {
            oLog.GenerarArchivoLog("Action [ActualizarMora] {listarAgencias} -- inicio");

            ListaAgencias lAgencias = new ListaAgencias();

            try
            {
                lAgencias = oLogic.listarAgencias("");
                ViewBag.UrlBase = UrlBase;
            }
            catch (Exception ex)
            {
                oLog.GenerarArchivoLog("Action [ActualizarMora - WEB] Error: "+ex.Message.ToString());
                throw ex;
            }
                return View(lAgencias);
        }

        [HttpPost]
        public ActionResult ActualizarNivelesMora(string sMoraActualizar = "")
        {

            int nFilas = 0;
            string cCodAgencia = string.Empty;
            decimal nMora = 0;
            String cUser = string.Empty;


            try
            {

                ResultadoObtenerModel<Usuario> usuarioSesion = (ResultadoObtenerModel<Usuario>)Session["UsuarioSCC"];
                cUser = usuarioSesion.Model.UsuarioWeb;

                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora] -- Inicio");

                if (!String.IsNullOrEmpty(sMoraActualizar))
                {
                    List<RequestActualizarMora> lMora = JsonConvert.DeserializeObject<List<RequestActualizarMora>>(sMoraActualizar);

                    foreach (RequestActualizarMora item in lMora)
                    {
                        cCodAgencia = item.cCodAgencia;
                        nMora = item.nMora;


                        oLog.GenerarArchivoLog("Action [ActualizarNivelesMora] ==> {cCodAgencia} : " + item.cCodAgencia + " - {nMora} : " + item.nMora + " ");

                        oLogic.ActualizarNivelesMora(item.cCodAgencia, item.nMora, cUser);
                        nFilas = nFilas + 1;

                    }
                }
                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora] -- FIN");

                RegistrarHitoricoMora();

                return Json(new { nEstado = 1, nFilas = nFilas });
            }
            catch (Exception ex)
            {
                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora - ERROR] ==> {cCodAgencia} : " + cCodAgencia + " - {nMora} : " + nMora + " {mensaje}: "+ex.Message.ToString());
                return Json(new { nEstado = 2, nFilas = nFilas });
                throw ex;
            }

        }

        public void RegistrarHitoricoMora()
        {
            try
            {
                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora {RegistrarHitoricoMora}] -- Inicio");
                oLogic.RegistrarHitoricoMora();
                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora {RegistrarHitoricoMora}] -- Fin");
            }
            catch (Exception ex)
            {
                oLog.GenerarArchivoLog("Action [ActualizarNivelesMora {RegistrarHitoricoMora}] -- Fin {mensaje}: "+ex.Message.ToString());
                throw ex;

            }
        }
    }
}