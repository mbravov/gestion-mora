﻿using Agrobanco.SGM.WEB.Comun;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGROBANCO.SGM.HELPERS.Log
{
    public class Log
    {
        public void GenerarArchivoLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = ApplicationKeys.rutaLogsError;
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("dd-MM-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string texto = fecha + " - " + strLog;
            log = new StreamWriter(fileStream);
            log.WriteLine(texto);
            log.Close();
        }
    }
}
