﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SGM.WEB.Helpers
{
    public class EloginResponse
    {
        public EloginResponse()
        {
            Agencias = new List<EAgencia>();

        }

        public string vToken { get; set; }
        public string vId { get; set; }
        public string vNombre { get; set; }
        public string vCargo { get; set; }
        public string vEmail { get; set; }
        public string vUsuarioWeb { get; set; }
        public string vUsuarioAD { get; set; }
        public string vEstadoAD { get; set; }
        public string vAgenciaAsignada { get; set; }
        public string vDescripcionAgencia { get; set; }
        public string vPassword { get; set; }
        public List<EAgencia> Agencias { get; set; }
        public string vCodFuncionario { get; set; }
        public string vPerfilDescripcion { get; set; }

        public EServicioResponse eServicioResponse { get; set; }
    }

    public class EServicioResponse
    {

        public EServicioResponse()
        {
            //Resultado = 1;
        }

        public int Resultado { get; set; }

        public string Mensaje { get; set; }
    }

    public class EUsuario
    {
        public string UsuarioWeb { get; set; }
        public string Token { get; set; }
    }
}