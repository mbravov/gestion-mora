﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SGM.WEB.Helpers
{
    public class EAgencia
    {
        public string vCodigo { get; set; }

        public string Valor { get; set; }
        public string Descripcion { get; set; }
        public string NombreRegional { get; set; }
    }
}