﻿using Agrobanco.SGM.WEB.Comun;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Agrobanco.SGM.WEB.Helpers
{
    public class SeguridadHelper
    {
        public ResultadoObtenerModel<Usuario> ObtenerSesionUsuario(string Token, string idPerfil)
        {
            try
            {
                string urlApi = $"/api/usuario/ObtenerSesionUsuario/{idPerfil}";

                //OBTENEMOS LA LISTA DE PERFILES
                ResultadoObtenerModel<Usuario> Resultado = SendRequest<ResultadoObtenerModel<Usuario>>
                                         (urlApi, null, Token, "GET", "application/json");

                return Resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public EloginResponse ObtenerUsuario(string usuarioWeb, string token)
        {
            string urlApi = $"/api/usuario/obtenerusuario";
            EUsuario usuarioRequest = new EUsuario
            {
                UsuarioWeb = usuarioWeb,
                Token = token
            };

            EloginResponse usuarioResponse = SendRequest<EloginResponse>
                                        (urlApi, usuarioRequest, token, "POST", "application/json");

            return usuarioResponse;

        }


        public bool ValidarTokenActivo(string Token)
        {
            bool validate = true;

            string urlApi = $"/api/validate/token";

            string Response = SendRequest<string>
                                     (urlApi, null, Token, "GET", "application/json");

            if (Response == "401" || Response == "ex")
            {
                validate = false;
            }

            return validate;

        }

        public ServicioResponse ValidarUsuarioAD(string Token, string usuarioAD)
        {

            string urlApi = $"/api/usuario/ValidarUsuarioAD?usuarioAD={usuarioAD}";
            ServicioResponse eServicioResponse = SendRequest<ServicioResponse>
                                                (urlApi, null, Token, "GET", "application/json");

            return eServicioResponse;
        }


        public static T SendRequest<T>(string requestUrl, object JSONRequest,
                                    string token, string tipoRequest, string JSONContentType)
        {
            requestUrl = ApplicationKeys.UrlAPISGS + requestUrl;
            T objResponse = default(T);

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(requestUrl);

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSONContentType));

                    if (!string.IsNullOrEmpty(token))
                        client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

                    HttpResponseMessage response = new HttpResponseMessage();
                    if (tipoRequest == "POST")
                    {
                        StringContent sc = new StringContent(JsonConvert.SerializeObject(JSONRequest), Encoding.UTF8, JSONContentType);
                        response = client.PostAsync(requestUrl, sc).Result;
                    }
                    else
                    {
                        response = client.GetAsync(requestUrl).Result;
                    }


                    //var message = response.Content.ReadAsStringAsync();
                    //objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                    //return objResponse;

                    if (response.IsSuccessStatusCode)
                    {
                        var message = response.Content.ReadAsStringAsync();
                        objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                        return objResponse;

                    }
                    else
                    {
                        objResponse = JsonConvert.DeserializeObject<T>("401");

                        return objResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //objResponse = JsonConvert.DeserializeObject<T>("ex");
                //return objResponse;
            }

        }
    }
}