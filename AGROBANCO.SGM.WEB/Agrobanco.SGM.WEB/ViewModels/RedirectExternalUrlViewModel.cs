﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SGM.WEB.ViewModels
{
    public class RedirectExternalUrlViewModel
    {
        public string urlApp { get; set; }
        public string urlWebSSA { get; set; }

    }
}